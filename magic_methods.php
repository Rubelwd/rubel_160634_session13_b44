<?php
//Construct and Destruct Magic Methods
Class Person{
    public $name = "test name",$phone = 0124563221,$dob = "12.12.12";

    public function __construct()
    {
        echo __METHOD__."I am inside the ".__METHOD__."<br>";
    }

    public static function doFrequently() {
        echo __METHOD__."I am doing it frequently<br>";
    }

    public function __destruct()
    {
       echo __METHOD__."I am dying, I am inside the ".__METHOD__."<br>";
    }
    public function __call($name, $arguments)
    {
       echo __METHOD__."I am inside the Name = ".__METHOD__."<br>";
       echo "Name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
        $file="error.txt";
        $myArray = array($name,$arguments);
        file_put_contents($file, serialize($myArray), FILE_APPEND);

    }

    public static function __callStatic($name, $arguments)
    {
        echo "I am inside the Name = ".__METHOD__."<br>";
        echo "Name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }

    public function __set($name, $value)
    {
        echo __METHOD__."Wrong property name = $name<br>";
        echo __METHOD__."Value tried to set to that wrong property = $value<br>";
    }

    public function __get($name)
    {
       echo __METHOD__."Wrong property name: $name<br>";
    }

    public function __isset($name)
    {
        echo __METHOD__."Wrong property Name: $name<br>";
    }

    public function __unset($name)
    {
        echo __METHOD__."Wrong property Name: $name<br>";
    }

    public function __sleep()
    {
        return array("name","phone");
    }
   /* public function __wakeup()
    {

    }*/

    public function __toString()
    {
        return "Are you crazy? I am just an object. Mind It.<br>";
    }

    public function __invoke()
    {
       echo "I am an object but you are call me ";
    }

}



Person::doFrequently();
Person::another_call("hello","Hi","whatsup");


$obj = new Person();
$obj ->error_method("name","age","profession");
$obj ->address = "goal pahar moor,ctg";
echo $obj ->address;

if(isset($obj->empty_property)) {

}
unset($obj->empty_property);

//Sleep and wakeup
$myVar = serialize($obj);
echo '<pre>';
var_dump($myVar);
echo "</pre>";

$myVar = serialize($obj);
echo '<pre>';
var_dump(unserialize($myVar));
echo "</pre>";

echo $obj;

